<?php
$pdo = require 'connect_db.php';

$genders = array(0 => "Nam", 1 => "Nữ");
$faculties = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

function reverse($i)
{
    if ($i == 0) {
        return 1;
    } else {
        return 0;
    }
}

session_start();
$_POST = $_SESSION;

if ($_SERVER["REQUEST_METHOD"] == 'POST') {
    $sql = 'INSERT INTO student(name, gender, faculty, birthday, address, avatar)
            VALUES(:name, :gender, :faculty, :birthday, :address, :avatar)';

    $statement = $pdo->prepare($sql);
    $statement->execute([
        ':name' => $_POST["name"],
        ':gender' => reverse($_POST["gender"]),
        ':faculty' => $_POST["faculty"],
        ':birthday' => date('Y-m-d H:i:s', strtotime(str_replace("/", "-", $_POST["birthday"]))),
        ':address' => $_POST["address"],
        ':avatar' => $_POST["picture"]
    ]);

    header('Location: complete_regist.php');
}

?>

<html>

<head>
    <meta charset="UTF-8">
    <title>day12 - confirm</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container submitted">
        <form method="post" class="form-wrapper">
            <div>
                <label for="name" class="form-label">Họ và tên</label>
                <label class="form-value"><?= $_POST["name"]; ?></label>
            </div>
            <div>
                <label for="gender" class="form-label">Giới tính</label>
                <label class="form-value"><?= $genders[$_POST["gender"]]; ?></label>
            </div>
            <div class="select-wrapper">
                <label for="faculty" class="form-label">Phân khoa</label>
                <label class="form-value"><?= $faculties[$_POST["faculty"]]; ?></label>
            </div>
            <div>
                <label for="birthday" class="form-label">Ngày sinh</label>
                <label class="form-value"><?= $_POST["birthday"]; ?></label>
            </div>
            <div>
                <label for="address" class="form-label">Địa chỉ</label>
                <label class="form-value"><?= $_POST["address"]; ?></label>
            </div>
            <div class="picture-wrapper">
                <label for="picture" class="form-label">Hình ảnh</label>
                <label class="form-value">
                    <img src="<?= $_POST["picture"] ?: "upload/blank.png"; ?>" alt="Hình ảnh" class="form-image">
                </label>
            </div>
            <input type="submit" value="Xác nhận" name="submit" class="submit-btn" />
        </form>
    </div>

</body>

</html>